const express = require('express');
const app = express();

app.use(express.static(__dirname + '/src/cliente/'));

app.listen('3000', function() {
  console.log(`El servidor se está ejecutando en http://localhost:3000/`);
});